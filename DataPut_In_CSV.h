//
// Created by Vlad-PC on 18.11.2021.
//

#ifndef LAB0B_DATAPUT_IN_CSV_H
#define LAB0B_DATAPUT_IN_CSV_H
#include "Read_TXT_In_String.h"

class DataPut_In_CSV {
public:
    void DataPut(std::map <std::string, int> &Word_Quantity,int &SUMMA);
};


#endif //LAB0B_DATAPUT_IN_CSV_H
