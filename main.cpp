#include <iostream>
#include <windows.h>
#include "Read_TXT_In_String.h"
#include "Editing_To_String.h"
#include "DataPut_In_CSV.h"
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    SetConsoleOutputCP(CP_UTF8);

    Read_TXT_In_String Rd;
    Rd.Read(Rd.FileStrings);
    Editing_To_String EdSt;
    EdSt.Editing(EdSt.Word_Quantity,EdSt.Sum_All_words,Rd.FileStrings);
    DataPut_In_CSV DP;
    DP.DataPut(EdSt.Word_Quantity,EdSt.Sum_All_words);
    return 0;
}