//
// Created by Vlad-PC on 18.11.2021.
//
#ifndef LAB0B_READ_TXT_IN_STRING_H
#define LAB0B_READ_TXT_IN_STRING_H
#include <iostream>
#include <fstream>
#include <string>
#include <wchar.h>
#include <map>

class Read_TXT_In_String {
public:
    std::string FileStrings;
    void Read(std::string &AcceptFile);
};


#endif //LAB0B_READ_TXT_IN_STRING_H
