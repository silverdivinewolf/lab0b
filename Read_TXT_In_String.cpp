//
// Created by Vlad-PC on 18.11.2021.
//
#include "Read_TXT_In_String.h"

void Read_TXT_In_String::Read(std::string &AcceptFile){
    std::string file_name;
    std::string line;
    AcceptFile="";
    const std::string End_Of_Line=" ";
    std::cout<<"Enter the location of your file.txt"<<std::endl;
    std::cin>>file_name;
    std::ifstream TXT_File(file_name);
    std::cout << "Open file.txt..." << std::endl;
    if (TXT_File.is_open())
    {
        while (getline(TXT_File, line)) {
            AcceptFile += line+End_Of_Line;
        }
        TXT_File.close();
        std::cout << "completed, file.txt close" << std::endl;
    }
    else{
        TXT_File.close();
        std::string YES;
        std::cout<<"...File not found"<<std::endl<<"to rerun the function? 'Yes' or end function"<<std::endl;
        std::cin>>YES;
        if(YES == "Yes")
            Read(AcceptFile);
        else
            std::cout<<"end Read function"<<std::endl;
    }
}