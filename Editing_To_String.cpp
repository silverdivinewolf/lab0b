//
// Created by Vlad-PC on 18.11.2021.
//

#include "Editing_To_String.h"

void Editing_To_String::Editing(std::map <std::string, int> &Word_Quantity,int &Sum_All_words,std::string &FileString){
    const int LengthStringZero=0;
    if(FileString.size()!=LengthStringZero) {
        const int LSide_1=65488;
        const int LSide_2=65489;
        const int RSide=1;
        const int A_rus=65424;
        const int n_rus=65471;
        const int YO_rus=65409;
        const int p_rus=65408;
        const int ya_rus=65423;
        const int yo_rus=65425;
        const int shiftFor_LSide_1=48;
        const int shiftFor_LSide_2=112;
        const int shiftFor_YO_yo=39;
        const int Сount_From_One=1;
        std::string word;
        bool Flag = false;
        for (int i = 0; i <= FileString.size(); i++) {
            wchar_t symbol = FileString[i];
            if ((FileString[i] >= 'a' && FileString[i] <= 'z') || (FileString[i] >= 'A' && FileString[i] <= 'Z') ||
                (FileString[i] >= '0' && FileString[i] <= '9')) {
                word += symbol;
            } else if (symbol == LSide_1) {
                symbol = FileString[i + RSide];
                if (symbol >= A_rus && symbol <= n_rus)
                    symbol = symbol + shiftFor_LSide_1;
                else if (symbol == YO_rus)
                    symbol = symbol + shiftFor_YO_yo;
                word += symbol;
                Flag = true;
            } else if (symbol == LSide_2) {
                symbol = FileString[i + RSide];
                if (symbol >= p_rus && symbol <= ya_rus)
                    symbol += shiftFor_LSide_2;
                else if (symbol == yo_rus)
                    symbol += shiftFor_YO_yo;
                word += symbol;
                Flag = true;
            } else {
                if (!Flag) {
                    if (!Word_Quantity[word] && word != "") {
                        Word_Quantity[word] = Сount_From_One;
                        word = "";
                    } else if (Word_Quantity[word] && word != "") {
                        Word_Quantity[word]++;
                        word = "";
                    }
                } else {
                    Flag = false;
                }
            }
        }
        if (!Word_Quantity[word] && word != "") {
            Word_Quantity[word] = 1;
            word = "";
        } else if (Word_Quantity[word] && word != "") {
            Word_Quantity[word]++;
            word = "";
        }
        const int Count_From_Scratch=0;
        Sum_All_words=Count_From_Scratch;
        std::map<std::string, int>::iterator it = Word_Quantity.begin();
        it++;
        for (; it != Word_Quantity.end(); it++) {
            Sum_All_words += it->second;
        }
    }
    else{
        std::cout<<"String not available"<<std::endl;
    }
}