//
// Created by Vlad-PC on 18.11.2021.
//

#include "DataPut_In_CSV.h"
void DataPut_In_CSV::DataPut(std::map <std::string, int> &Word_Quantity,int &Sum_All_words) {
    std::string file_name;
    std::cout << "Enter the location of your file.csv" << std::endl;
    std::cin >> file_name;
    std::ifstream Prov(file_name);
    if (Prov.is_open()) {
        Prov.close();
        const int percent=100;
        std::ofstream CSV_File(file_name);
        std::cout << "Open file.csv..." << std::endl;
        std::map<std::string, int>::iterator it = Word_Quantity.begin();
        it++;
        for (; it != Word_Quantity.end(); it++) {
            CSV_File << it->first << ";" << it->second << ";" << percent * double(it->second) / Sum_All_words << "%" << std::endl;
        }
        CSV_File.close();
        std::cout << "completed, file.csv close" << std::endl;
    } else {
        Prov.close();
        std::string YES;
        std::cout << "...File not found" << std::endl << "to rerun the function? 'Yes' or end function"
                  << std::endl;
        std::cin >> YES;
        if (YES == "Yes")
            DataPut(Word_Quantity, Sum_All_words);
        else
            std::cout << "end Read function" << std::endl;
    }
}