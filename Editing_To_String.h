//
// Created by Vlad-PC on 18.11.2021.
//

#ifndef LAB0B_EDITING_TO_STRING_H
#define LAB0B_EDITING_TO_STRING_H
#include "Read_TXT_In_String.h"

class Editing_To_String {
public:
    std::map <std::string, int> Word_Quantity;
    int Sum_All_words;
    void Editing(std::map <std::string, int> &Word_Quantity,int &Sum_All_words,std::string &line);
};


#endif //LAB0B_EDITING_TO_STRING_H
